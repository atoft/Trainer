# Location issues
## No location
Make sure to allow location in the popup on first launch, or allow it from the system settings.

## Location seems erratic
Try disabling wi-fi as the location service will try to use the location of hotspots
in the absence of a GPS signal.

## No GPS location
Currently, devices require a SIM card inserted to use the GPS hardware. [(Related isue)](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/issues/183)

On PinePhone (TODO and Librem5?) getting an initial GPS fix can take a very long time. See the following for more info:
- https://forum.pine64.org/showthread.php?tid=11680
- https://gist.github.com/alastair-dm/263209b54d01209be28828e555fa6628

TODO test workaround.
