/*
 * Copyright 2021 Alastair Toft
 *
 * This file is part of Trainer.
 *
 * Trainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Trainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Trainer. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Trainer {
    [GtkTemplate (ui = "/dev/atoft/trainer/window.ui")]
    public class MainWindow : Hdy.ApplicationWindow {
    	[GtkChild]
    	Gtk.Stack main_stack;

    	[GtkChild]
    	Hdy.ViewSwitcherBar main_switcher;

		uint inhibit_cookie = 0;

        public void inhibit (bool should_inhibit) {
            if (should_inhibit) {
                const Gtk.ApplicationInhibitFlags wanted_flags = Gtk.ApplicationInhibitFlags.IDLE | Gtk.ApplicationInhibitFlags.SUSPEND;

                uint returned_cookie = application.inhibit (this, wanted_flags, "Stop the device from sleeping while tracking an activity.");

                if (returned_cookie == 0) {
                    stderr.printf ("Inhibit failed\n");
                    return;
                }

                inhibit_cookie = returned_cookie;
            }
            else {
                application.uninhibit (inhibit_cookie);
            }

        }

        // TODO Need an interface for views?
        public void open_secondary_view (Gtk.Widget view) {
            main_switcher.set_reveal (false);

            // Looks kind of weird - new view shows in switcher as it hides.
            // Better to use separate child or Leaflet?
            view.show ();
            main_stack.add_child (view);
            main_stack.set_visible_child (view);
        }

        public void close_secondary_view (Gtk.Widget view) {
            main_stack.remove (view);
        }

        public Application get_app () {
        	return (Application) application;
        }

		public MainWindow (Application app) {
			Object (application: app);

            set_title ("Trainer");

            TrackView track_view = new TrackView (this);
            track_view.show ();
            main_stack.add_titled (track_view, "track_view", "Track");
		}
    }
}
