/*
 * Copyright 2021 Alastair Toft
 *
 * This file is part of trainer.
 *
 * trainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * trainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with trainer. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Trainer {
    public class ActivityTracker {
        LocationService location_service = null;
        Activity activity = null;
        GLib.Timer timer;

        public void start (LocationService service) {
            if (activity != null) {
                warning ("Activity already in progress.\n");
                return;
            }

            location_service = service;
            activity = new Activity ();
            timer = new GLib.Timer ();

            // Treat the latest location as the first point.
            add_point (location_service.latest_location, 0.0);

            timer.start ();

            location_service.on_location_updated.connect (on_location_updated);
            print ("ActivityTracker: Activity in progress...\n");
        }

        // TODO support pausing.

        public Activity finish () {
            location_service.on_location_updated.disconnect (on_location_updated);
            location_service = null;

            timer.stop ();

            activity.duration_seconds = timer.elapsed ();

            write_activity ();

            Activity finished_activity = activity;
            activity = null;

            print("ActivityTracker: Finished.\n");
            return finished_activity;
        }

        public void get_progress (out double time, out double distance) {
            if (activity == null) {
                time = 0.0;
                distance = 0.0;
            }
            else {
                time = timer.elapsed ();
                distance = activity.total_distance;
            }
        }

        void on_location_updated (LocationInfo location) {
            // TODO if time since last location > threshold
            // and distance from previous < threshold

            double elapsed = timer.elapsed ();
            add_point (location, elapsed);
        }

        void add_point (LocationInfo location, double elapsed) {
            ActivityPoint point = ActivityPoint ();
            point.location = location;
            point.elapsed_seconds = elapsed;

            if (activity.points.size > 0) {
                ActivityPoint previous = activity.points[activity.points.size - 1];

                point.segment_distance = distance_between (previous.location, point.location);
            }
            else {
                point.segment_distance = 0.0;
            }

            activity.total_distance += point.segment_distance;
            activity.points.add (point);
        }

        void write_activity () {
            assert (activity != null);

            string activity_json = activity.to_json ();

            DateTime now = new DateTime.now_local ();
            string date_string = now.format ("%Y-%m-%d-%H-%M-%S");

            string file_name = "activity-%s.json".printf (date_string);
            string path = "%s/%s".printf(Environment.get_user_data_dir (), file_name);
            print (@"Writing activity to $path\n");

            try {
                File file = File.new_for_path (path);

                DataOutputStream stream = new DataOutputStream (file.create (FileCreateFlags.REPLACE_DESTINATION));

                stream.put_string (activity_json);
            }
            catch (Error e) {
                stderr.printf ("Failed to write the activity: %s\n", e.message);
            }
        }
    }
}
