/*
 * Copyright 2021 Alastair Toft
 *
 * This file is part of trainer.
 *
 * trainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * trainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with trainer. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Trainer {
	const string BUS_NAME_DESKTOP_PORTAL = "org.freedesktop.portal.Desktop";
	const string OBJECT_NAME_DESKTOP_PORTAL = "/org/freedesktop/portal/desktop";
	const string OBJECT_NAME_PREFIX_SESSION_PORTAL = "/org/freedesktop/portal/desktop/session/";

	const string INTERFACE_NAME_LOCATION_PORTAL = "org.freedesktop.portal.Location";
	const string METHOD_NAME_LOCATION_PORTAL_CREATE_SESSION = "org.freedesktop.portal.Location.CreateSession";
	const string METHOD_NAME_LOCATION_PORTAL_START = "org.freedesktop.portal.Location.Start";
	const string SIGNAL_NAME_LOCATION_PORTAL_UPDATED = "LocationUpdated";
}
