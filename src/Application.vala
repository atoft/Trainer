/*
 * Copyright 2021 Alastair Toft
 *
 * This file is part of trainer.
 *
 * trainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * trainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with trainer. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Trainer {

    public class Application : Gtk.Application {
        public ActivityTracker activity_tracker;
    	public LocationService location_service;

        public Application() {
            Object (application_id: "dev.atoft.Trainer");

            activity_tracker = new ActivityTracker ();
            location_service = new LocationService ();
        }

		public override void startup () {
			base.startup();
			Hdy.init();
			register_actions ();
		}

        public override void activate () {
            var window = new MainWindow(this);
            window.show ();
        }

        private void register_actions ()
	    {
		    var about_action = new GLib.SimpleAction ("about", null);
		    about_action.activate.connect (parameter => this.show_about_dialog ());
		    this.add_action (about_action);

		    var quit_action = new GLib.SimpleAction ("quit", null);
		    quit_action.activate.connect (parameter => this.quit ());
		    this.set_accels_for_action ("app.quit", { "<Primary>Q" });
		    this.add_action (quit_action);
	    }

	    private void show_about_dialog ()
		    requires (this.get_active_window () != null)
	    {
		    var about_dialog = new Gtk.AboutDialog ()
		    {
                transient_for = this.active_window,
                modal = true,
                destroy_with_parent = true,
                // Translators: This is the title of the About dialog
                title = "About Trainer",
                logo_icon_name = "",
                version = "",
                // Translators: This is the summary of the app
                comments = "Track running, walking or other activities using your mobile device.",
                website = "https://codeberg.org/atoft/Trainer",
                // Translators: This is the label of the link to the app's repository
                website_label = "Project repository",
                copyright = "© 2021 Alastair Toft",
                license_type = Gtk.License.GPL_3_0,
                authors = { "Alastair Toft <me@atoft.dev>" }
		    };

		    about_dialog.present_with_time (Gdk.CURRENT_TIME);
	    }
    }
}
