/*
 * Copyright 2021 Alastair Toft
 *
 * This file is part of trainer.
 *
 * trainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * trainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with trainer. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Trainer {
    // TODO Does struct prevent heap alloc?
    public struct ActivityPoint {
        LocationInfo location;
        double elapsed_seconds;
        double segment_distance;
    }

    public class Activity {
        public Gee.ArrayList<ActivityPoint?> points;
        public double duration_seconds;
        public double total_distance;

        public Activity () {
            points = new Gee.ArrayList<ActivityPoint?>();
            duration_seconds = 0.0;
        }

        public string to_json () {
            Json.Builder builder = new Json.Builder ();

	        builder.begin_object ();
	        builder.set_member_name ("duration_seconds");
	        builder.add_double_value (duration_seconds);

	        builder.set_member_name ("total_distance");
	        builder.add_double_value (total_distance);

	        builder.set_member_name ("points");
	        builder.begin_array ();

	        foreach (ActivityPoint? point in points) {
	            builder.begin_object ();
	            builder.set_member_name ("latitude");
	            builder.add_double_value (point.location.latitude);
	            builder.set_member_name ("longitude");
	            builder.add_double_value (point.location.longitude);
	            builder.set_member_name ("elapsed_seconds");
	            builder.add_double_value (point.elapsed_seconds);
	            builder.set_member_name ("segment_distance");
	            builder.add_double_value (point.segment_distance);
	            builder.end_object ();
	        }

	        builder.end_array ();

	        builder.end_object ();

	        Json.Generator generator = new Json.Generator ();
	        Json.Node root = builder.get_root ();
	        generator.set_root (root);

	        string str = generator.to_data (null);

	        return str;
        }
    }
}
