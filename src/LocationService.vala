/*
 * Copyright 2021 Alastair Toft
 *
 * This file is part of trainer.
 *
 * trainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * trainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with trainer. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Trainer {
	public class LocationTimestamp {
		public uint64 seconds;
		public uint64 microseconds;
	}

	public class LocationInfo {
		public double latitude;
		public double longitude;
		public double altitude;
		public double accuracy;
		public double speed;
		public LocationTimestamp timestamp;

		// TODO Any way to autogenerate in Vala?
		public string to_string() {
			// Just lat-long for this test.
			return @"$latitude,$longitude";
		}
    }

    public double to_radians (double degrees) {
        return degrees * Math.PI / 180.0;
    }

    // https://en.wikipedia.org/wiki/Haversine_formula
    public double distance_between (LocationInfo a, LocationInfo b) {
        double lat1 = to_radians (a.latitude);
        double lat2 = to_radians (b.latitude);

        double long1 = to_radians (a.longitude);
        double long2 = to_radians (b.longitude);

        double angle_squared =
            Math.pow (Math.sin ((lat2 - lat1) / 2.0), 2.0) + Math.cos (lat1) * Math.cos (lat2) * Math.pow (Math.sin ((long2 - long1) / 2.0), 2.0);

        double angle = Math.sqrt (angle_squared);

        const double EARTH_RADIUS_METERS = 6372.8 * 1000.0;

        double dist = 2.0 * EARTH_RADIUS_METERS * Math.asin (angle);

        return dist;
    }

	public class LocationService {
	    const double ACCURACY_THRESHOLD = 500.0;

	    bool has_started = false;

		// A new location is available.
		public signal void on_location_updated (LocationInfo info);

		// A location is ignored for some reason.
		public signal void on_location_discarded ();

		public LocationInfo latest_location;

		public bool enable_location () {
		    if (has_started) {
		        return true;
		    }

		    has_started = true;

			try {
				var location_proxy = new GLib.DBusProxy.for_bus_sync (
					GLib.BusType.SESSION,
					GLib.DBusProxyFlags.NONE,
					null,
					BUS_NAME_DESKTOP_PORTAL,
					OBJECT_NAME_DESKTOP_PORTAL,
					INTERFACE_NAME_LOCATION_PORTAL);

				// Generate a unique token for our session (https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Session)
				string session_token = "trainer%u".printf (GLib.Random.next_int());
				print ("%s\n", session_token);

				string name = location_proxy.g_connection.unique_name;
				print ("%s\n", name);
				name = name.replace (":", "");
				name = name.replace(".", "_");
				print ("%s\n", name);

				StringBuilder session_id_builder = new StringBuilder ();
				session_id_builder.append (OBJECT_NAME_PREFIX_SESSION_PORTAL);
				session_id_builder.append (name);
				session_id_builder.append ("/");
				session_id_builder.append (session_token);
				string session_id = session_id_builder.str;
				print ("%s\n", session_id);  // TODO don't actually need to construct it in this case, should be able to use the result of CreateSession.

				var create_session_array_builder = new GLib.VariantBuilder (VariantType.ARRAY);
				create_session_array_builder.add ("{sv}", "session_handle_token", new Variant.string(session_token));

				var create_session_args = new GLib.Variant ("(a{sv})", create_session_array_builder);

				GLib.Variant session_handle = location_proxy.call_sync(METHOD_NAME_LOCATION_PORTAL_CREATE_SESSION, create_session_args, DBusCallFlags.NONE, -1);
				print("%s\n", session_handle.print(true));


				// TODO Why can't we receive the signal from the proxy directly? We'd need to make a new proxy for the session object.
				//location_proxy.g_signal.connect((sender_name, signal_name, parameters) => {
				//        print("%s\n", parameters.print(true));
				//    });

				location_proxy.g_connection.signal_subscribe(BUS_NAME_DESKTOP_PORTAL, INTERFACE_NAME_LOCATION_PORTAL, SIGNAL_NAME_LOCATION_PORTAL_UPDATED, OBJECT_NAME_DESKTOP_PORTAL, null, DBusSignalFlags.NONE,
					(connection, sender_name, object_path, interface_name, signal_name, parameters) => {
						on_portal_location_updated (parameters);
					});

				var start_array_builder = new GLib.VariantBuilder (VariantType.ARRAY);
				start_array_builder.add ("{sv}", "handle_token", new Variant.string(session_token));
				var start_args = new GLib.Variant ("(osa{sv})", new GLib.ObjectPath(session_id), "", start_array_builder);
				location_proxy.call_sync (METHOD_NAME_LOCATION_PORTAL_START, start_args, DBusCallFlags.NONE, -1);

				return true;
			}
			catch (IOError e) {
				stderr.printf ("%s\n", e.message);
				return false;
			}
			catch (GLib.Error e) {
				stderr.printf ("%s\n", e.message);
				return false;
			}
		}

		double read_double (Variant dictionary, string name) {
			GLib.Variant? variant = dictionary.lookup_value (name, GLib.VariantType.DOUBLE);

			if (variant == null) {
				warning("Location variant entry couldn't be found for %s\n", name);
				return 0.0;
			}

			if (!variant.is_of_type(VariantType.DOUBLE)) {
				warning("Location variant %s wasn't of the correct type.\n", name);
				return 0.0;
			}

			return variant.get_double ();
		}

		LocationTimestamp read_timestamp (Variant dictionary, string name) {
			LocationTimestamp result = new LocationTimestamp ();

			GLib.Variant? tuple = dictionary.lookup_value (name, GLib.VariantType.TUPLE);

			if (tuple == null) {
				warning("Location variant entry couldn't be found for %s\n", name);
				return result;
			}

			if (!tuple.is_of_type(VariantType.TUPLE)) {
				warning("Location variant %s wasn't of the correct type.\n", name);
				return result;
			}

			// TODO Error handling.
			result.seconds = tuple.get_child_value(0).get_uint64 ();
			result.microseconds = tuple.get_child_value(0).get_uint64 ();
			return result;
		}

		void on_portal_location_updated (GLib.Variant parameters) {
			if (parameters.n_children () < 2) {
				warning("Location variant is missing children.\n");
				return;
			}

			GLib.Variant dictionary = parameters.get_child_value(1);

			LocationInfo location = new LocationInfo ();
			location.latitude = read_double (dictionary, "Latitude");
			location.longitude = read_double (dictionary, "Longitude");
			location.altitude = read_double (dictionary, "Altitude");
			location.accuracy = read_double (dictionary, "Accuracy");
			location.speed = read_double (dictionary, "Speed");
			location.timestamp = read_timestamp (dictionary, "Timestamp");

			string location_string = location.to_string();

			print ("%s (%f)\n", location_string, location.accuracy);

			if (location.accuracy < ACCURACY_THRESHOLD) {
			    on_location_updated (location);
			    // TODO Draw accuracy on the map, similar to https://gitlab.gnome.org/GNOME/gnome-maps/-/blob/master/src/userLocationMarker.js

			    latest_location = location;
		    }
		    else {
		        on_location_discarded ();
		    }
		}
	}
}
