/*
 * Copyright 2021 Alastair Toft
 *
 * This file is part of Trainer.
 *
 * Trainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Trainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Trainer. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Trainer {
    [GtkTemplate (ui = "/dev/atoft/trainer/views/ActivityInProgressView.ui")]
    public class ActivityInProgressView : Gtk.Box {
        [GtkChild]
        Gtk.Label time;

        [GtkChild]
        Gtk.Label distance;

        [GtkChild]
        Gtk.Button btn_finish;

        MainWindow window;
        ActivityTracker activity_tracker;
        uint tick_id = 0;

        public ActivityInProgressView (MainWindow parent, ActivityTracker tracker) {
            window = parent;
            activity_tracker = tracker;

            tick_id = add_tick_callback ((c) => {
                on_tick();
                return true;
            });

            btn_finish.clicked.connect (on_clicked_finish);
        }

        void on_tick () {
            double elapsed_seconds;
            double total_distance;
            activity_tracker.get_progress (out elapsed_seconds, out total_distance);

            time.set_label ("%.0fs".printf(elapsed_seconds));
            distance.set_label ("%.0fm".printf(total_distance));
        }

        void on_clicked_finish () {
            remove_tick_callback (tick_id);
            Activity activity = activity_tracker.finish ();

            window.inhibit (false);
            window.close_secondary_view (this);

            ActivityDetailsView details_view = new ActivityDetailsView (activity);
            window.open_secondary_view (details_view);
        }
    }
}
