/*
 * Copyright 2021 Alastair Toft
 *
 * This file is part of Trainer.
 *
 * Trainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Trainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Trainer. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Trainer {
    [GtkTemplate (ui = "/dev/atoft/trainer/views/TrackView.ui")]
    public class TrackView : Gtk.Box {
        [GtkChild]
        Gtk.Widget waiting_interface;

        [GtkChild]
        Gtk.Widget ready_interface;

        [GtkChild]
        Gtk.Box map_box;

        [GtkChild]
        Gtk.Button btn_start_activity;

        Shumate.View map_view;
        Shumate.MarkerLayer map_marker_layer;
		Shumate.Marker map_current_position_marker;

		MainWindow window;

        public TrackView (MainWindow parent) {
            window = parent;

            btn_start_activity.clicked.connect (on_click_start_activity);
            btn_start_activity.set_sensitive (false);

            // TODO Why doesn't the waiting widget expand to fill the space?
            ready_interface.set_visible (false);
            waiting_interface.set_visible (true);

            map_view = new Shumate.View.simple ();

            map_box.append (map_view);

            map_view.get_viewport ().zoom_level = 16;

            const double DEFAULT_LATITUDE = 52.202160;
            const double DEFAULT_LONGITUDE = 0.128180;

            // TODO At location from last session?
            map_view.center_on (DEFAULT_LATITUDE, DEFAULT_LONGITUDE);
            map_view.kinetic_mode = true;

            // TODO No scrolling? No zooming?

            map_marker_layer = new Shumate.MarkerLayer (map_view.get_viewport());

            map_current_position_marker = new Shumate.Marker ();
            var emblem_image = new Gtk.Image.from_icon_name ("mark-location-symbolic");
            emblem_image.insert_after (map_current_position_marker, null);
            map_current_position_marker.set_location (DEFAULT_LATITUDE, DEFAULT_LONGITUDE);

            map_marker_layer.add_marker (map_current_position_marker);

            map_view.add_layer(map_marker_layer);
            map_view.show ();

            window.get_app ().location_service.on_location_updated.connect (on_location_updated);
            window.get_app ().location_service.on_location_discarded.connect (on_location_discarded);

            bool is_location_started = window.get_app ().location_service.enable_location ();

            if (!is_location_started) {
                print ("Location failed\n");
                // TODO In app message.
            }
        }

        void on_location_updated (LocationInfo info) {
            btn_start_activity.set_sensitive (true);
            ready_interface.set_visible (true);
            waiting_interface.set_visible (false);

            map_view.center_on (info.latitude, info.longitude);
            map_current_position_marker.set_location (info.latitude, info.longitude);

            // TODO Draw accuracy on the map, similar to https://gitlab.gnome.org/GNOME/gnome-maps/-/blob/master/src/userLocationMarker.js
        }

        void on_location_discarded () {
            print("Location discarded\n");
        }

        void on_click_start_activity () {
            ActivityTracker tracker = window.get_app ().activity_tracker;

            window.inhibit (true);
            tracker.start (window.get_app ().location_service);

            var in_progress_view = new ActivityInProgressView (window, tracker);
            window.open_secondary_view (in_progress_view);
        }
    }
}
