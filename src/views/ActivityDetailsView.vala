/*
 * Copyright 2021 Alastair Toft
 *
 * This file is part of Trainer.
 *
 * Trainer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Trainer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Trainer. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Trainer {
    [GtkTemplate (ui = "/dev/atoft/trainer/views/ActivityDetailsView.ui")]
    public class ActivityDetailsView : Gtk.Box {
        [GtkChild]
        Gtk.Box map_box;

        Shumate.View map_view;
        Shumate.MarkerLayer map_marker_layer;

        Activity activity = null;

        public ActivityDetailsView (Activity activity_to_view) {
            activity = activity_to_view;

            map_view = new Shumate.View.simple ();

            map_box.append (map_view);

            // TODO Based on route?
            map_view.get_viewport ().zoom_level = 16;

            map_marker_layer = new Shumate.MarkerLayer (map_view.get_viewport());

            double lat_sum = 0.0;
            double long_sum = 0.0;

            foreach (ActivityPoint? point in activity.points) {
                lat_sum += point.location.latitude;
                long_sum += point.location.longitude;

                Shumate.Marker position_marker = new Shumate.Marker ();

                var emblem_image = new Gtk.Image.from_icon_name ("mark-location-symbolic");
                emblem_image.insert_after (position_marker, null);

                position_marker.set_location (point.location.latitude, point.location.longitude);

                map_marker_layer.add_marker (position_marker);

                // TODO Draw a line.
            }

            map_view.center_on (lat_sum / activity.points.size, long_sum / activity.points.size);
            map_view.kinetic_mode = true;

            map_view.add_layer(map_marker_layer);
            map_view.show ();
        }
    }
}

